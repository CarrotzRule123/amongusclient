import { Color, PlayerColor, PColorMap, PlayerData, SpriteSheet, PlayerImages } from "./data"
import { Sprite } from "./sprite"

export class Assets {
    private canvas: HTMLCanvasElement
    public player: {[key: string]: PlayerImages}
    public map: CanvasImageSource
    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas
        this.player = {}
    }

    public async loadAll(): Promise<void> {
        const pdata = await this.loadJson("./player.json") as PlayerData
        const shapes = await this.loadSpriteSheet("./sprites.json")
        const sheet = await this.loadImage("./Player.png")

        const bmps = await this.setColors(sheet, pdata)
        const ctx = this.canvas.getContext("2d")
        ctx.drawImage(bmps["red"], 0, 0);
        for (const color in bmps) {
            const bmp = bmps[color];
            const colorSheet = {}
            for (const key in pdata.sheet) {
                const sprites = []
                for (const img of pdata.sheet[key]) {
                    sprites.push(shapes[img])
                }
                colorSheet[key] = new Sprite(this.canvas, bmp, sprites, 10)
            }
            this.player[color] = colorSheet as PlayerImages
        }
        this.map = await this.loadImage("./Map.png")
        console.log("Loaded")
    }

    public async loadJson(path: string): Promise<object> {
        const response = await fetch(path);
        const json = await response.json();
        return json;
    }

    public async loadSpriteSheet(path: string): Promise<SpriteSheet> {
        const response = await fetch(path);
        const json = await response.json();
        const sheet = {}
        for (const sprite of json) {
            const {x, y, width, height} = sprite
            sheet[sprite.name] = {x, y, width, height}
        }
        return sheet
    }

    public async loadImage(path: string): Promise<HTMLImageElement> {
        return new Promise((resolve) => {
            const image = new Image();
            image.src = path;
            image.onload = (): void => resolve(image);
        });
    }

    private async setColors(img: HTMLImageElement, pdata: PlayerData) {
        const ctx = this.canvas.getContext("2d")
        const { width, height } = img
        this.canvas.width = width
        this.canvas.height = height
        ctx.drawImage(img, 0, 0);
        const data = ctx.getImageData(0, 0, width, height);
        const res = {}
        for (const key in pdata.color) {
            res[key] = new Uint8ClampedArray(data.data)
        }

        for (let i = 0; i < data.data.length; i += 4) {
            const r = data.data[i];
            const g = data.data[i + 1]
            const b = data.data[i + 2]
            if (!(r == g && r == b)) {
                let def = [r, g, b] as Color
                if (b > r && b > g) {
                    this.setColor(def, pdata, "body", res, i)
                } else if (r > b && r > g) {
                    this.setColor(def, pdata, "head", res, i)
                } else if (g > b && g > r) {
                    this.setColor(def, pdata, "light", res, i)
                } else if (g > b && g > r && b > 160) {
                    this.setColor(def, pdata, "dark", res, i)
                }
            }
        }
        ctx.clearRect(0, 0, width, height)

        const bitmaps = {}
        for (const key in res) {
            const imgdata = new ImageData(res[key], width, height)
            bitmaps[key] = await createImageBitmap(imgdata)
        }
        return bitmaps
    }

    private setColor(def: Color, pdata: PlayerData, key: keyof PlayerColor, res: PColorMap, j: number) {
        const base = pdata.base[key]
        for (const color in pdata.color) {
            const shade = pdata.color[color][key]
            for (let i = 0; i < 3; i++) {
                res[color][i + j] = def[i] - base[i] + shade[i]
            }
        }
    }
}