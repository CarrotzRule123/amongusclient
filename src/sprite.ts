import { ImageSize } from "./data"

export class Sprite {
    private canvas: HTMLCanvasElement
    private image: ImageBitmap
    private sprites: ImageSize[]
    private count: number = 0
    private freq: number
    private clock: number = 0

    constructor(canvas: HTMLCanvasElement, image: ImageBitmap, sprites: ImageSize[] = [], freq: number = 0) {
        this.canvas = canvas
        this.image = image
        this.sprites = sprites
        this.freq = freq
    }

    public render(px: number, py: number) {
        const ctx = this.canvas.getContext("2d")
        const { x, y, height, width } = this.sprites[this.count]
        ctx.translate(-width / 2, -height / 2)
        ctx.drawImage(this.image, x, y, width, height, px, py, width, height)
        this.clock += 1
        if (this.clock >= this.freq) {
            this.clock = 0
            this.count += 1
        }
        if (this.count >= this.sprites.length) {
            this.count = 0
        }
    }

    public renderSprite(px: number, py: number) {
        const ctx = this.canvas.getContext("2d")
        const { x, y, height, width } = this.sprites[0]
        ctx.translate(-width / 2, -height / 2)
        ctx.drawImage(this.image, x, y, width, height, px, py, width, height)
    }

    public sprite() {
        return this.sprites[this.count]
    }
}