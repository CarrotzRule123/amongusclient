import { Assets } from "./assets"
import { Render } from "./render"
import { Listen } from "./listen"
import { Player } from "./player"

const canvas = document.getElementById("canvas") as HTMLCanvasElement

const listen = new Listen()
const assets = new Assets(canvas)
const render = new Render(assets)
const player = new Player({x: 4500, y: 1500})

async function load() {
    await assets.loadAll()
    document.getElementById("loading").style.display = "none"
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    setInterval(update, 1000/ 100)
}

function update() {
    const ctx = canvas.getContext("2d")
    ctx.clearRect(0, 0, canvas.height, canvas.width)
    player.update(listen)
    render.update(player)
}

window.onload = load