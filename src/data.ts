import { Sprite } from "./sprite"

export type Point = {
    x: number,
    y: number
}

export type PlayerData = {
    base: PlayerColor,
    color: {[key: string]: PlayerColor},
    sheet: PlayerSprites
}

export type PlayerSprites = {
    [anim in PlayerAnims]: string[]
}

export type PlayerImages = {
    [anim in PlayerAnims]: Sprite
}

export type SpriteSheet = {
    [key: string]: ImageSize
}

export enum PlayerAnims {
    walk = "walk",
    player = "player",
    death = "death"
}

export type PlayerColor = {
    body: Color,
    head: Color,
    light: Color,
    dark: Color
}

export type Color = [number, number, number]

export type ImageSize = {
    x: number,
    y: number,
    height: number,
    width: number
}

export type PColorMap = {[color: string]: ImageData}

export type PlayerColors = {[key: string]: PlayerColor}
