export class Listen {
    public mouse:{x: number, y: number}
    public direction:number
    public keys = {}

    constructor(){
        this.direction = 0
        this.mouse = {x:0, y:0}
        document.onmousemove = this.mouseMove.bind(this)
        document.onkeydown = this.keyDown.bind(this)
        document.onkeyup = this.keyUp.bind(this)
    }

    private mouseMove(e: MouseEvent){
        this.mouse.x = e.clientX; 
        this.mouse.y = e.clientY
        this.direction = this.mouseDir()
    }

    private keyDown(e: KeyboardEvent){
        this.keys[e.code] = true
    }

    private keyUp(e: KeyboardEvent){
        this.keys[e.code] = false
    }

    private mouseDir(){
        return (Math.atan2(
            window.innerHeight / 2 - this.mouse.y,
            window.innerWidth / 2 - this.mouse.x
        ));
    }
}