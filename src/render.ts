import { Assets } from "./assets"
import { Player } from "./player"
import { Point } from "./data"

export class Render {
    private canvas:HTMLCanvasElement
    private ctx:CanvasRenderingContext2D
    private assets:Assets
    
    constructor(assets:Assets){
        this.assets = assets
        this.canvas = document.getElementById("canvas") as HTMLCanvasElement
        this.ctx = this.canvas.getContext("2d")
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
    }

    public update(player: Player){
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.renderBackground(player.position)
        this.renderPlayer(player.position, player)
    }

    private renderPlayer(me: Point, player: Player) {
        const { x, y } = player.position;
        const canvasX = this.canvas.width / 2 + x - me.x;
        const canvasY = this.canvas.height / 2 + y - me.y;
        const color = "red"
        const walk = this.assets.player[color].walk
        const stand = this.assets.player[color].player

        this.ctx.save();
        this.ctx.translate(canvasX, canvasY);
        if(player.direction) {
            this.ctx.scale(-1, 1);
        }
        if (player.walking) {
            walk.render(0, 0)
        } else {
            stand.renderSprite(0, 0)
        }
        this.ctx.restore();
    }

    private renderBackground(me: Point){
        const {x, y}  = me
        const mapX = -x + this.canvas.width / 2
        const mapY = -y + this.canvas.height / 2
    
        this.ctx.drawImage(this.assets.map, mapX, mapY)
    }

}