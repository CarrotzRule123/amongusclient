import { Listen } from "./listen"
import { Point } from "./data"

const constants = {
    SPEED: 15
}

export class Player {
    public direction: boolean
    public position: Point
    public walking: boolean

    constructor(position: Point) {
        this.direction = true
        this.position = position
    }

    update(listen: Listen) {
        this.move(listen)
    }

    move(listen: Listen) {
        this.walking = false
        if (listen.keys["ArrowUp"]) {
            this.position.y -= constants.SPEED
            this.walking = true
        }
        if (listen.keys["ArrowDown"]) {
            this.position.y += constants.SPEED
            this.walking = true
        }
        if (listen.keys["ArrowRight"]) {
            this.position.x += constants.SPEED
            this.direction = false
            this.walking = true
        }
        if (listen.keys["ArrowLeft"]) {
            this.position.x -= constants.SPEED
            this.direction = true
            this.walking = true
        }
    }
}
